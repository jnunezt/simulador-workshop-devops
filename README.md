## Levantar wordpress con opciones basicas

docker-compose up 

## Levantar worpdress con opcion de background

docker-compose up -d

## mirar el log del servicio

docker-compose logs -f

## bajar el servicio

docker-compose down 

## crear virtual machine con docker

docker-machine create --driver virtualbox default

## listar maquinas virtuales

docker-machine ls

## conectarse a maquina virtual con env 

eval "$(docker-machine env default)"

## Conocer mi ip de machine

docker-machine ip default